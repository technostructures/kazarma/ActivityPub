# SPDX-License-Identifier: AGPL-3.0-only

defmodule ActivityPubWeb.WebFingerController do
  use ActivityPubWeb, :controller

  alias ActivityPub.WebFinger

  plug(ActivityPubWeb.Plugs.SetFormatPlug)

  def webfinger(%{assigns: %{format: format}} = conn, %{"resource" => resource})
      when format in ["xml", "xrd+xml"] do
    with {:ok, response} <- WebFinger.webfinger(resource, "XML") do
      conn
      |> put_resp_content_type("application/xrd+xml")
      |> send_resp(200, response)
    else
      _e ->
        send_resp(conn, 404, "Couldn't find user")
    end
  end

  def webfinger(%{assigns: %{format: format}} = conn, %{"resource" => resource})
      when format in ["json", "jrd+json"] do
    with {:ok, response} <- WebFinger.webfinger(resource, "JSON") do
      json(conn, response)
    else
      _e ->
        conn
        |> put_status(404)
        |> json("Couldn't find user")
    end
  end

  def webfinger(conn, _params), do: send_resp(conn, 400, "Bad Request")
end
